# Open-Scrum
============

# Arquitetura Apolo

Este projeto é uma iniciativa open source que baseou sua arquitetura no projeto Apolo-Base (https://bitbucket.org/olimpo/apolo-base).

## Faça sua cópia

O Open Scrum é versionado com git, portanto, para fazer sua cópia do código e controlá-la, será necessário ter o git em sua máquina.

Após ter instalado o git, para pegar sua cópia basta utilizar o seguinte comando:

```
$ git clone https://bitbucket.org/olimpo/open-scrum.git
```

Para usuário Windows, eu indico o seguinte client git: [http://msysgit.github.com/](http://msysgit.github.com/)

Há algum tempo eu fiz um "tutorialzinho" de alguns comandos git e para que eles servem: [http://macielbombonato.blogspot.com.br/2012/02/utilizando-git-versionador-local.html](http://macielbombonato.blogspot.com.br/2012/02/utilizando-git-versionador-local.html)

## Crie seu Fork deste projeto

Caso queira montar um projeto a partir deste e queira deixá-lo versionado aqui no bitbucket, use a opção "Fork" que encontra-se no topo da tela. O processo é rápido e o passo a passo é bem simples.

Espero que este projeto lhe seja útil e tendo sugestões ou encontrando problemas, por favor, entre em contato ou abra um incidente aqui no bitbucket.

# Licença
---------

Open Scrum é um software de código aberto, você pode redistribuí-lo e/ou
modificá-lo conforme a licença Apache versão 2.0. Veja o arquivo LICENSE-Apache.txt
