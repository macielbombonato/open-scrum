package com.openscrum.data.repository;

import com.openscrum.data.model.Release;

public interface ReleaseRepository extends BaseRepository<Release, Long> {

}
