package com.openscrum.data.repository;

import com.openscrum.data.model.Workflow;

public interface WorkflowRepository extends BaseRepository<Workflow, Long> {

}
