package com.openscrum.data.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.openscrum.common.util.InputLength;
import com.openscrum.data.entitylistener.AuditLogListener;

@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "t_user_story")
@AttributeOverride(name = "id", column = @Column(name = "user_story_id"))
public class UserStory extends AuditableBaseEntity {
	private static final long serialVersionUID = 8431905262860757880L;

	@Column(name = "name", length = InputLength.NAME, nullable = false, unique = true)
	private String name;

	@Column(name = "description", length = InputLength.DESCR)
	private String description;

	@Column(name = "priority", nullable = false)
	private Integer priority;

	@Column(name = "story_points")
	private Double storyPoints;

	@ManyToOne
	@JoinColumn(name = "project_id", nullable = false)
	private Project project;

	@ManyToOne
	@JoinColumn(name = "release_id", nullable = true)
	private Release release;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Double getStoryPoints() {
		return storyPoints;
	}

	public void setStoryPoints(Double storyPoints) {
		this.storyPoints = storyPoints;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Release getRelease() {
		return release;
	}

	public void setRelease(Release release) {
		this.release = release;
	}

}
