package com.openscrum.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.openscrum.business.exception.WorkflowParserException;
import com.openscrum.business.model.SearchResult;
import com.openscrum.business.service.WorkflowService;
import com.openscrum.common.exception.AccessDeniedException;
import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.Project_;
import com.openscrum.data.model.Workflow;
import com.openscrum.data.model.Workflow_;
import com.openscrum.security.SecuredEnum;
import com.openscrum.security.UserPermission;
import com.openscrum.web.enums.Navigation;
import com.openscrum.web.model.DataTableParamModel;
import com.openscrum.web.model.DataTableResultModel;

@Controller
@RequestMapping(value = "/workflow")
public class WorkflowController extends CrudController<Workflow> {

	@Autowired
	private WorkflowService workflowService;

	@SecuredEnum(UserPermission.WORKFLOW_LIST)
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@Override
	public ModelAndView list(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.workflow.list"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.WORKFLOW_LIST.getPath());

		List<Workflow> workflowList = workflowService.list();

		mav.addObject("workflowList", workflowList);

		return mav;
	}

	@SecuredEnum({ UserPermission.WORKFLOW_CREATE, UserPermission.WORKFLOW_EDIT })
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@Override
	public ModelAndView save(@Valid Workflow workflow, BindingResult result, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();

		if (result.hasErrors()) {
			mav.setViewName(getRedirectionPath(request, Navigation.WORKFLOW_NEW, Navigation.WORKFLOW_EDIT));

			mav.addObject("workflow", workflow);
			mav.addObject("readOnly", false);
			mav.addObject("error", true);

			StringBuilder message = new StringBuilder();
			for (ObjectError error : result.getAllErrors()) {
				DefaultMessageSourceResolvable argument = (DefaultMessageSourceResolvable) error.getArguments()[0];
				message.append(MessageBundle.getMessageBundle("common." + argument.getDefaultMessage()) + ": " + error.getDefaultMessage() + "\n <br />");
			}
			mav.addObject("message", message.toString());

		} else if (workflow != null) {
			try {
				workflow = workflowService.save(workflow);

				mav = view(workflow.getId(), request);
				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
			} catch (AccessDeniedException | WorkflowParserException e) { // TODO fix messages
				mav = list(request);
				mav.addObject("error", true);
				mav.addObject("message", e.getCustomMsg());
			}
		}

		return mav;
	}

	@SecuredEnum(UserPermission.WORKFLOW_CREATE)
	@RequestMapping(value = "new", method = RequestMethod.GET)
	@Override
	public ModelAndView create(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.workflow.new"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.WORKFLOW_NEW.getPath());

		Workflow workflow = new Workflow();

		workflow.setCreatedBy(getAuthenticatedUser());
		workflow.setCreationDate(new Date());

		workflow.setLastUpdatedBy(getAuthenticatedUser());
		workflow.setLastUpdateDate(new Date());

		mav.addObject("workflow", workflow);
		mav.addObject("readOnly", false);

		return mav;
	}

	@SecuredEnum(UserPermission.WORKFLOW_EDIT)
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	@Override
	public ModelAndView edit(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.workflow.edit"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.WORKFLOW_EDIT.getPath());

		Workflow workflow = workflowService.find(id);

		workflow.setLastUpdatedBy(getAuthenticatedUser());
		workflow.setLastUpdateDate(new Date());

		mav.addObject("workflow", workflow);
		mav.addObject("readOnly", false);

		return mav;
	}

	@SecuredEnum(UserPermission.WORKFLOW_REMOVE)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	@ResponseBody
	@Override
	public String remove(@PathVariable Long id) {
		String result = "";

		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();

		Workflow workflow = workflowService.find(id);

		if (workflow != null) {
			// TODO check associated projects
			try {
				workflowService.remove(workflow);

				result = MessageBundle.getMessageBundle("common.msg.remove.success");
				jsonItem.put("success", true);
			} catch (Throwable e) {
				result = MessageBundle.getMessageBundle("common.remove.msg.error");
				jsonItem.put("success", false);
			}
		}

		jsonItem.put("message", result);
		jsonSubject.accumulate("result", jsonItem);

		return jsonSubject.toString();
	}

	@SecuredEnum(UserPermission.WORKFLOW_LIST)
	@RequestMapping(value = "search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public DataTableResultModel search(@ModelAttribute DataTableParamModel param, HttpServletRequest request) {
		String sortColIndex = request.getParameter("iSortCol_0");
		String sortDir = request.getParameter("sSortDir_0");

		SearchResult<Workflow> searchResult = workflowService.search(param.getsSearch(), param.getiDisplayLength(), param.getiDisplayStart(),
				getSortField(sortColIndex), getSortDir(sortDir));

		DataTableResultModel result = new DataTableResultModel();
		result.setsEcho(param.getsEcho());
		result.setiTotalDisplayRecords(searchResult.getPageCount());
		result.setiTotalRecords(searchResult.getResults() != null ? searchResult.getResults().size() : 0);

		if (searchResult.getResults() != null) {
			String[][] rows = new String[searchResult.getResults().size()][4];
			int index = 0;
			for (Workflow workflow : searchResult.getResults()) {
				String[] cols = new String[4];
				cols[0] = workflow.getName();
				cols[1] = getActions(workflow.getId(), Navigation.WORKFLOW.getPath(), "ROLE_ADMIN, ROLE_WORKFLOW_LIST", "ROLE_ADMIN, ROLE_WORKFLOW_EDIT",
						"ROLE_ADMIN, ROLE_WORKFLOW_REMOVE", request);
				rows[index++] = cols;
			}
			result.setAaData(rows);
		} else {
			result.setAaData(new String[][] {});
		}

		return result;
	}

	private String getSortField(String sIndex) {
		if (StringUtils.hasText(sIndex)) {
			int index = Integer.parseInt(sIndex);
			switch (index) {
				case 0:
					return Workflow_.name.getName();
			}
		}
		return Project_.name.getName();
	}

	@RequestMapping(value = "test", method = RequestMethod.GET)
	@ResponseBody
	public List<Workflow> test() {

		return workflowService.list();
	}

	@SecuredEnum({ UserPermission.WORKFLOW_VIEW, UserPermission.WORKFLOW_LIST })
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.workflow"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.WORKFLOW_VIEW.getPath());

		Workflow workflow = workflowService.find(id);

		mav.addObject("workflow", workflow);
		mav.addObject("readOnly", true);

		return mav;
	}
}
