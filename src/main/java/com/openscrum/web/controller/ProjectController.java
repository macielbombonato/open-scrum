package com.openscrum.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import net.sf.json.JSONObject;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.openscrum.business.model.SearchResult;
import com.openscrum.business.service.ProjectService;
import com.openscrum.business.service.UserService;
import com.openscrum.common.exception.AccessDeniedException;
import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.Project;
import com.openscrum.data.model.Project_;
import com.openscrum.data.model.User;
import com.openscrum.security.SecuredEnum;
import com.openscrum.security.UserPermission;
import com.openscrum.web.enums.Navigation;
import com.openscrum.web.model.DashboardLink;
import com.openscrum.web.model.DataTableParamModel;
import com.openscrum.web.model.DataTableResultModel;

@Controller
@RequestMapping(value = "/project")
public class ProjectController extends CrudController<Project> {

	@Autowired
	private ProjectService projectService;
	@Autowired
	private UserService userService;

	@SecuredEnum(UserPermission.PROJECT_LIST)
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@Override
	public ModelAndView list(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.project.list"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.PROJECT_LIST.getPath());

		List<Project> projectList = projectService.list();

		mav.addObject("projectList", projectList);

		return mav;
	}

	@SecuredEnum({ UserPermission.PROJECT_CREATE, UserPermission.PROJECT_EDIT })
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@Override
	public ModelAndView save(@Valid Project project, BindingResult result, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();

		if (result.hasErrors()) {
			mav.setViewName(getRedirectionPath(request, Navigation.PROJECT_NEW, Navigation.PROJECT_EDIT));

			mav.addObject("project", project);
			mav.addObject("readOnly", false);
			mav.addObject("error", true);

			StringBuilder message = new StringBuilder();
			for (ObjectError error : result.getAllErrors()) {
				DefaultMessageSourceResolvable argument = (DefaultMessageSourceResolvable) error.getArguments()[0];
				message.append(MessageBundle.getMessageBundle("common." + argument.getDefaultMessage()) + ": " + error.getDefaultMessage() + "\n <br />");
			}
			mav.addObject("message", message.toString());

		} else if (project != null) {
			try {
				project = projectService.save(project);

				mav = view(project.getId(), request);
				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
			} catch (AccessDeniedException e) {
				mav = list(request);
				mav.addObject("error", true);
				mav.addObject("message", e.getCustomMsg());
			}
		}

		return mav;
	}

	@SecuredEnum({ UserPermission.PROJECT_CREATE, UserPermission.USER_LIST })
	@RequestMapping(value = "new", method = RequestMethod.GET)
	@Override
	public ModelAndView create(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.project.new"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.PROJECT_NEW.getPath());

		Project project = new Project();

		project.setCreatedBy(getAuthenticatedUser());
		project.setCreationDate(new Date());

		project.setLastUpdatedBy(getAuthenticatedUser());
		project.setLastUpdateDate(new Date());

		List<User> userList = userService.list();

		mav.addObject("userList", userList);
		mav.addObject("project", project);
		mav.addObject("readOnly", false);

		return mav;
	}

	@SecuredEnum({ UserPermission.USER_PERMISSION_VIEW, UserPermission.USER_PERMISSION_LIST, UserPermission.USER_LIST })
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.project"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.PROJECT_VIEW.getPath());

		Project project = projectService.find(id);
		project.getUsers();
		List<User> userList = userService.list();

		mav.addObject("userList", userList);
		mav.addObject("project", project);
		mav.addObject("readOnly", true);

		return mav;
	}

	@SecuredEnum(UserPermission.PROJECT_EDIT)
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	@Override
	public ModelAndView edit(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.project.edit"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.PROJECT_EDIT.getPath());

		Project project = projectService.find(id);
		
		project.setLastUpdatedBy(getAuthenticatedUser());
		project.setLastUpdateDate(new Date());

		List<User> userList = userService.list();

		mav.addObject("userList", userList);
		mav.addObject("project", project);
		mav.addObject("readOnly", false);

		return mav;
	}

	@SecuredEnum(UserPermission.PROJECT_REMOVE)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	@ResponseBody
	@Override
	public String remove(@PathVariable Long id) {
		String result = "";

		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();

		Project project = projectService.find(id);

		if (project != null) {
			// TODO check associated backlogs
			try {
				projectService.remove(project);

				result = MessageBundle.getMessageBundle("common.msg.remove.success");
				jsonItem.put("success", true);
			} catch (Throwable e) {
				result = MessageBundle.getMessageBundle("common.remove.msg.error");
				jsonItem.put("success", false);
			}
		}

		jsonItem.put("message", result);
		jsonSubject.accumulate("result", jsonItem);

		return jsonSubject.toString();
	}

	@SecuredEnum(UserPermission.PROJECT_LIST)
	@RequestMapping(value = "search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public DataTableResultModel search(@ModelAttribute DataTableParamModel param, HttpServletRequest request) {
		String sortColIndex = request.getParameter("iSortCol_0");
		String sortDir = request.getParameter("sSortDir_0");

		SearchResult<Project> searchResult = projectService.search(param.getsSearch(), param.getiDisplayLength(), param.getiDisplayStart(),
				getSortField(sortColIndex), getSortDir(sortDir));

		DataTableResultModel result = new DataTableResultModel();
		result.setsEcho(param.getsEcho());
		result.setiTotalDisplayRecords(searchResult.getPageCount());
		result.setiTotalRecords(searchResult.getResults() != null ? searchResult.getResults().size() : 0);

		if (searchResult.getResults() != null) {
			String[][] rows = new String[searchResult.getResults().size()][4];
			int index = 0;
			for (Project project : searchResult.getResults()) {
				String[] cols = new String[4];
				cols[0] = project.getName();
				cols[1] = getActions(project.getId(), Navigation.PROJECT.getPath(), "ROLE_ADMIN, ROLE_PROJECT_LIST", "ROLE_ADMIN, ROLE_PROJECT_EDIT",
						"ROLE_ADMIN, ROLE_PROJECT_REMOVE", request);
				rows[index++] = cols;
			}
			result.setAaData(rows);
		} else {
			result.setAaData(new String[][] {});
		}

		return result;
	}

	@SecuredEnum(UserPermission.PROJECT_LIST)
	@RequestMapping(value = "listProjects", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<DashboardLink> listProjects() {
		List<DashboardLink> result = new ArrayList<DashboardLink>();
		List<Project> projects = projectService.list();
		if (projects != null) {
			for (Project proj : projects) {
				DashboardLink link = new DashboardLink();
				BeanUtils.copyProperties(proj, link);

				result.add(link);
			}
		}
		return result;
	}

	private String getSortField(String sIndex) {
		if (StringUtils.hasText(sIndex)) {
			int index = Integer.parseInt(sIndex);
			switch (index) {
				case 0:
					return Project_.name.getName();
			}
		}
		return Project_.name.getName();
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Set.class, "users", new CustomCollectionEditor(Set.class) {
			@Override
			protected Object convertElement(Object element) {
				Long id = null;

				if (element instanceof String && !((String) element).equals("")) {
					// From the JSP 'element' will be a String
					try {
						id = Long.parseLong((String) element);
					} catch (NumberFormatException e) {
						log.error("Element was " + ((String) element), e);
					}
				} else if (element instanceof Long) {
					// From the database 'element' will be a Long
					id = (Long) element;
				}

				return id != null ? userService.find(id) : null;
			}
		});
	}

}
