package com.openscrum.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.openscrum.business.model.SearchResult;
import com.openscrum.business.service.AuditLogService;
import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.AuditLog;
import com.openscrum.data.model.AuditLog_;
import com.openscrum.security.SecuredEnum;
import com.openscrum.security.UserPermission;
import com.openscrum.web.enums.Navigation;
import com.openscrum.web.model.DataTableParamModel;
import com.openscrum.web.model.DataTableResultModel;

@Controller
@RequestMapping(value = "/auditlog")
public class AuditLogController extends CrudController<AuditLog> {

	@Autowired
	AuditLogService auditLogService;

	@Override
	@SecuredEnum(UserPermission.ADMIN)
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.auditlog.list"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.AUDIT_LOG_LIST.getPath());

		return mav;
	}

	@SecuredEnum(UserPermission.ADMIN)
	@RequestMapping(value = "search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public DataTableResultModel search(@ModelAttribute DataTableParamModel param, HttpServletRequest request) {
		String sortColIndex = request.getParameter("iSortCol_0");
		String sortDir = request.getParameter("sSortDir_0");

		SearchResult<AuditLog> searchResult = auditLogService.search(param.getsSearch(), param.getiDisplayLength(), param.getiDisplayStart(),
				getSortField(sortColIndex), getSortDir(sortDir));

		DataTableResultModel result = new DataTableResultModel();
		result.setsEcho(param.getsEcho());
		result.setiTotalDisplayRecords(searchResult.getPageCount());
		result.setiTotalRecords(searchResult.getResults() != null ? searchResult.getResults().size() : 0);

		if (searchResult.getResults() != null) {
			String[][] rows = new String[searchResult.getResults().size()][4];
			int index = 0;
			for (AuditLog auditLog : searchResult.getResults()) {
				String[] cols = new String[5];

				cols[0] = auditLog.getTransactionType().getName();
				cols[1] = auditLog.getEntityName();
				cols[2] = auditLog.getRegistryId() + "";
				cols[3] = auditLog.getOperationDate().toString();
				cols[4] = auditLog.getExecutedBy().getName();

				rows[index++] = cols;
			}
			result.setAaData(rows);
		} else {
			result.setAaData(new String[][] {});
		}

		return result;
	}

	private String getSortField(String sIndex) {
		if (StringUtils.hasText(sIndex)) {
			int index = Integer.parseInt(sIndex);
			switch (index) {
				case 1:
					return AuditLog_.entityName.getName();
			}
		}
		return AuditLog_.entityName.getName();
	}

	@Override
	public ModelAndView save(AuditLog entity, BindingResult result, HttpServletRequest request) {
		return null;
	}

	@Override
	public ModelAndView create(HttpServletRequest request) {
		return null;
	}

	@Override
	public ModelAndView edit(Long id, HttpServletRequest request) {
		return null;
	}

	@Override
	public String remove(Long id) {
		return null;
	}
}
