package com.openscrum.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.openscrum.business.model.SearchResult;
import com.openscrum.business.service.UserGroupService;
import com.openscrum.common.exception.AccessDeniedException;
import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.UserGroup;
import com.openscrum.data.model.UserGroup_;
import com.openscrum.security.SecuredEnum;
import com.openscrum.security.UserPermission;
import com.openscrum.web.enums.Navigation;
import com.openscrum.web.model.DataTableParamModel;
import com.openscrum.web.model.DataTableResultModel;

@Controller
@RequestMapping(value = "/user-group")
public class UserGroupController extends CrudController<UserGroup> {

	@Autowired
	private UserGroupService userGroupService;

	@SecuredEnum({ UserPermission.USER_PERMISSION_CREATE, UserPermission.USER_PERMISSION_EDIT })
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("userGroup") UserGroup userGroup, BindingResult result, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();

		if (userGroup != null) {
			try {
				userGroupService.save(userGroup);

				mav = view(userGroup.getId(), request);
				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
			} catch (AccessDeniedException e) {
				mav = list(request);
				mav.addObject("error", true);
				mav.addObject("message", e.getCustomMsg());
			}
		}

		return mav;
	}

	@SecuredEnum(UserPermission.USER_PERMISSION_LIST)
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.usergroup.list"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_PERMISSION_LIST.getPath());

		List<UserGroup> userGroupList = userGroupService.list();

		mav.addObject("userGroupList", userGroupList);

		return mav;
	}

	@SecuredEnum(UserPermission.USER_PERMISSION_CREATE)
	@RequestMapping(value = "new", method = RequestMethod.GET)
	public ModelAndView create(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.usergroup.new"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_PERMISSION_NEW.getPath());

		UserGroup userGroup = new UserGroup();

		userGroup.setCreatedBy(getAuthenticatedUser());
		userGroup.setCreationDate(new Date());

		userGroup.setLastUpdatedBy(getAuthenticatedUser());
		userGroup.setLastUpdateDate(new Date());

		mav.addObject("userGroup", userGroup);
		mav.addObject("permissionList", userGroupService.getUserPermissionList(getAuthenticatedUser()));
		mav.addObject("readOnly", false);

		return mav;
	}

	@SecuredEnum(UserPermission.USER_PERMISSION_EDIT)
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.usergroup.edit"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_PERMISSION_EDIT.getPath());

		UserGroup userGroup = userGroupService.find(id);

		userGroup.setLastUpdatedBy(getAuthenticatedUser());
		userGroup.setLastUpdateDate(new Date());

		mav.addObject("userGroup", userGroup);
		mav.addObject("permissionList", userGroupService.getUserPermissionList(getAuthenticatedUser()));
		mav.addObject("readOnly", false);

		return mav;
	}

	@Override
	@SecuredEnum(UserPermission.USER_PERMISSION_REMOVE)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	public @ResponseBody
	String remove(@PathVariable Long id) {
		String result = "";

		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();

		UserGroup userGroup = userGroupService.find(id);

		if (userGroup != null) {
			if (userGroup.getUsers() != null && !userGroup.getUsers().isEmpty()) {
				result = MessageBundle.getMessageBundle("user.group.msg.error.has.associated.users");
				jsonItem.put("success", false);
			} else {
				try {
					userGroupService.remove(userGroup);

					result = MessageBundle.getMessageBundle("common.msg.remove.success");
					jsonItem.put("success", true);
				} catch (Throwable e) {
					result = MessageBundle.getMessageBundle("common.remove.msg.error");
					jsonItem.put("success", false);
				}
			}
		}

		jsonItem.put("message", result);
		jsonSubject.accumulate("result", jsonItem);

		return jsonSubject.toString();
	}

	@SecuredEnum({ UserPermission.USER_PERMISSION_VIEW, UserPermission.USER_PERMISSION_LIST, UserPermission.USER_LIST })
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.usergroup"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_PERMISSION_VIEW.getPath());

		UserGroup userGroup = userGroupService.find(id);

		mav.addObject("userGroup", userGroup);
		mav.addObject("readOnly", true);

		return mav;
	}

	@SecuredEnum(UserPermission.USER_PERMISSION_LIST)
	@RequestMapping(value = "search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public DataTableResultModel search(@ModelAttribute DataTableParamModel param, HttpServletRequest request) {
		String sortColIndex = request.getParameter("iSortCol_0");
		String sortDir = request.getParameter("sSortDir_0");

		SearchResult<UserGroup> searchResult = userGroupService.search(param.getsSearch(), param.getiDisplayLength(), param.getiDisplayStart(),
				getSortField(sortColIndex), getSortDir(sortDir));

		DataTableResultModel result = new DataTableResultModel();
		result.setsEcho(param.getsEcho());
		result.setiTotalDisplayRecords(searchResult.getPageCount());
		result.setiTotalRecords(searchResult.getResults() != null ? searchResult.getResults().size() : 0);

		if (searchResult.getResults() != null) {
			String[][] rows = new String[searchResult.getResults().size()][4];
			int index = 0;
			for (UserGroup userGroup : searchResult.getResults()) {
				String[] cols = new String[4];
				cols[0] = userGroup.getName();

				StringBuffer sbPerms = new StringBuffer();
				sbPerms.append("<table class=\"table table-condensed table-bordered\">");
				sbPerms.append("<tbody>");
				for (UserPermission userPermission : userGroup.getPermissions()) {
					sbPerms.append("<tr><td>");
					sbPerms.append(userPermission.getAttribute());
					sbPerms.append("</td></tr>");

				}
				sbPerms.append("</tbody></table>");

				cols[1] = sbPerms.toString();
				cols[2] = getActions(userGroup.getId(), Navigation.USER_PERMISSION.getPath(), "ROLE_ADMIN, ROLE_PERMISSION_LIST",
						"ROLE_ADMIN, ROLE_PERMISSION_EDIT", "ROLE_ADMIN, ROLE_PERMISSION_REMOVE", request);
				rows[index++] = cols;
			}
			result.setAaData(rows);
		} else {
			result.setAaData(new String[][] {});
		}

		return result;
	}

	private String getSortField(String sIndex) {
		if (StringUtils.hasText(sIndex)) {
			int index = Integer.parseInt(sIndex);
			switch (index) {
				case 0:
					return UserGroup_.name.getName();
			}
		}
		return UserGroup_.name.getName();
	}

}
