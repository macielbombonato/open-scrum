package com.openscrum.web.controller;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.openscrum.business.model.FileContent;
import com.openscrum.business.model.SearchResult;
import com.openscrum.business.service.FileService;
import com.openscrum.business.service.ProjectService;
import com.openscrum.business.service.UserGroupService;
import com.openscrum.business.service.UserService;
import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.User;
import com.openscrum.data.model.UserGroup;
import com.openscrum.data.model.User_;
import com.openscrum.security.SecuredEnum;
import com.openscrum.security.UserPermission;
import com.openscrum.web.enums.Navigation;
import com.openscrum.web.model.DataTableParamModel;
import com.openscrum.web.model.DataTableResultModel;

@Controller
@RequestMapping(value = "/user")
public class UserController extends CrudController<User> {

	private final String ACCEPTED_FILE_TYPE = ".gif.jpg.png";

	@Autowired
	private UserService userService;
	@Autowired
	private FileService<User> fileService;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private ProjectService projectService;

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.user"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_INDEX.getPath());

		mav.addObject("user", getAuthenticatedUser());
		mav.addObject("readOnly", true);
		return mav;
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "change-password", method = RequestMethod.GET)
	public ModelAndView changePassword(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.user.changepassword"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_CHANGE_PASSWORD.getPath());

		mav.addObject("user", getAuthenticatedUser());
		mav.addObject("readOnly", true);
		mav.addObject("changePassword", true);
		return mav;
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "change-password-save", method = RequestMethod.POST)
	public ModelAndView changePasswordSave(@ModelAttribute("user") User user, HttpServletRequest request,
			@RequestParam(defaultValue = "") String passwordConfirmation) {
		ModelAndView mav = index(request);

		if (entityHasErrors(user, true, passwordConfirmation)) {
			mav.setViewName(getRedirectionPath(request, Navigation.USER_CHANGE_PASSWORD, Navigation.USER_CHANGE_PASSWORD));
			mav.addObject("user", getAuthenticatedUser());
			mav.addObject("readOnly", true);
			mav.addObject("changePassword", true);
			mav.addObject("error", true);

			StringBuilder message = new StringBuilder();
			message.append(additionalValidation(user, true, passwordConfirmation));

			mav.addObject("message", message.toString());

			return mav;
		}

		if (user != null) {
			User dbuser = userService.find(user.getId());

			dbuser.setPassword(user.getPassword());

			userService.save(dbuser, true, null);
		}

		return mav;
	}

	@SecuredEnum(UserPermission.USER_CREATE)
	@RequestMapping(value = "new", method = RequestMethod.GET)
	public ModelAndView create(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.user.new"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_NEW.getPath());

		User user = new User();

		user.setCreatedBy(getAuthenticatedUser());
		user.setCreationDate(new Date());

		user.setLastUpdatedBy(getAuthenticatedUser());
		user.setLastUpdateDate(new Date());

		mav.addObject("user", user);
		mav.addObject("groupList", userGroupService.list());
		mav.addObject("readOnly", false);

		return mav;
	}

	@SecuredEnum(UserPermission.USER_EDIT)
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.user.edit"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_EDIT.getPath());

		User user = userService.find(id);

		user.setLastUpdatedBy(getAuthenticatedUser());
		user.setLastUpdateDate(new Date());

		mav.addObject("user", user);
		mav.addObject("groupList", userGroupService.list());
		mav.addObject("readOnly", false);
		mav.addObject("editing", true);

		return mav;
	}

	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.user"), 2, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_VIEW.getPath());

		User user = userService.find(id);

		mav.addObject("user", user);
		mav.addObject("readOnly", true);

		return mav;
	}

	@Override
	@SecuredEnum(UserPermission.USER_REMOVE)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	public @ResponseBody
	String remove(@PathVariable Long id) {
		String result = "";

		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();

		User user = userService.find(id);

		if (user != null) {
			if (!getAuthenticatedUser().equals(user)) {
				try {
					userService.remove(user);

					result = MessageBundle.getMessageBundle("common.msg.remove.success");
					jsonItem.put("success", true);
				} catch (Throwable e) {
					result = MessageBundle.getMessageBundle("common.remove.msg.error");
					jsonItem.put("success", false);
				}
			} else {
				result = MessageBundle.getMessageBundle("user.msg.error.remove.yourself");
				jsonItem.put("success", false);
			}
		}

		jsonItem.put("message", result);
		jsonSubject.accumulate("result", jsonItem);

		return jsonSubject.toString();
	}

	/**
	 * Use <code>save(User user,BindingResult result, HttpServletRequest request , boolean changePassword, String passwordConfirmation)</code>.
	 */
	@Override
	public ModelAndView save(User entity, BindingResult result, HttpServletRequest request) {
		return null;
	}

	@SecuredEnum({ UserPermission.USER_CREATE, UserPermission.USER_EDIT })
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute("user") User entity, BindingResult result, HttpServletRequest request,
			@RequestParam(defaultValue = "false") boolean changePassword, @RequestParam(defaultValue = "") String passwordConfirmation) {
		ModelAndView mav = new ModelAndView();

		MultipartFile objectFile = null;

		List<MultipartFile> files = null;

		if (entity.getPicturefiles() != null) {
			files = entity.getPicturefiles();
		}

		if (files != null && !files.isEmpty()) {
			for (MultipartFile multipartFile : files) {
				objectFile = multipartFile;
			}
		}

		if (objectFile != null && objectFile.getOriginalFilename() != null && !objectFile.getOriginalFilename().isEmpty()) {
			entity.setPictureOriginalName(objectFile.getOriginalFilename());
			entity.setPictureGeneratedName(objectFile.getOriginalFilename());
		}

		/*
		 * Object validation
		 */
		if (result.hasErrors() || entityHasErrors(entity, changePassword, passwordConfirmation)) {
			mav.setViewName(getRedirectionPath(request, Navigation.USER_NEW, Navigation.USER_EDIT));
			mav.addObject("user", entity);
			mav.addObject("groupList", userGroupService.list());
			mav.addObject("readOnly", false);
			mav.addObject("error", true);

			/*
			 * Specific validation to show or not the password field
			 */
			String referer = request.getHeader("referer");
			if (referer != null && referer.contains(Navigation.USER_EDIT.getPath())) {
				mav.addObject("editing", true);
			}

			StringBuilder message = new StringBuilder();
			for (ObjectError error : result.getAllErrors()) {
				DefaultMessageSourceResolvable argument = (DefaultMessageSourceResolvable) error.getArguments()[0];

				message.append(MessageBundle.getMessageBundle("common.field") + " " + MessageBundle.getMessageBundle("user." + argument.getDefaultMessage())
						+ ": " + error.getDefaultMessage() + "\n <br />");
			}

			message.append(additionalValidation(entity, changePassword, passwordConfirmation));

			mav.addObject("message", message.toString());

			return mav;
		}

		if (entity != null) {
			FileContent file = null;

			if (objectFile != null) {
				file = new FileContent();
				file.setFile(objectFile);
			}

			userService.save(entity, changePassword, file);

			mav = view(entity.getId(), request);

			mav.addObject("msg", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
		}

		return mav;
	}

	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request) {
		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.user.list"), 1, request);

		ModelAndView mav = new ModelAndView(Navigation.USER_LIST.getPath());

		return mav;
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "current-project", method = RequestMethod.POST)
	@ResponseBody
	public String updateCurrentProject(@RequestBody String projectId, HttpServletRequest request) {
//		User loggedUser = userService.getAuthenticatedUser();
//		Project currentProject = projectService.find(Long.parseLong(projectId.substring(projectId.indexOf("=") + 1)));
//		if (currentProject != null) {
//			if (loggedUser.getProjects().contains(currentProject)) {
//				loggedUser.setCurrentProject(currentProject);
//				loggedUser = userService.save(loggedUser);
//
//				return currentProject.getName();
//			}
//		}
		return null;
	}

	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public DataTableResultModel search(@ModelAttribute DataTableParamModel param, HttpServletRequest request) {

		String sortColIndex = request.getParameter("iSortCol_0");
		String sortDir = request.getParameter("sSortDir_0");

		SearchResult<User> searchResult = userService.search(param.getsSearch(), param.getiDisplayLength(), param.getiDisplayStart(),
				getSortField(sortColIndex), getSortDir(sortDir));

		DataTableResultModel result = new DataTableResultModel();
		result.setsEcho(param.getsEcho());
		result.setiTotalDisplayRecords(searchResult.getPageCount());
		result.setiTotalRecords(searchResult.getResults() != null ? searchResult.getResults().size() : 0);

		if (searchResult.getResults() != null) {
			String[][] rows = new String[searchResult.getResults().size()][4];
			int index = 0;
			for (User user : searchResult.getResults()) {
				String[] cols = new String[5];

				if (StringUtils.hasText(user.getPictureGeneratedName())) {
					cols[0] = "<img class=\"img-thumbnail\" src=\"" + request.getContextPath() + "/uploadedfiles/User/" + user.getId() + "/"
							+ user.getPictureGeneratedName() + "\" style=\"width: 50px;height: 50px;\"/>";
				} else {
					cols[0] = "<span class=\"glyphicon glyphicon-user\"></span>";
				}

				cols[1] = user.getName();
				cols[2] = user.getEmail();

				StringBuffer sbGroups = new StringBuffer();
				sbGroups.append("<table class=\"table table-condensed table-bordered\">");
				sbGroups.append("<tbody>");
				for (UserGroup userGroup : user.getGroups()) {
					if (request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_USER_PERMISSION_VIEW")) {
						sbGroups.append("<tr><td>");
						sbGroups.append("<a href=\"");
						sbGroups.append(request.getContextPath());
						sbGroups.append("/user-group/view/");
						sbGroups.append(userGroup.getId());
						sbGroups.append("\"");
						sbGroups.append(" class=\"btn btn-link\">");
						sbGroups.append(userGroup.getName());
						sbGroups.append("</a></td></tr>");
					}
				}
				sbGroups.append("</tbody></table>");

				cols[3] = sbGroups.toString();
				cols[4] = getActions(user.getId(), Navigation.USER.getPath(), "ROLE_ADMIN, ROLE_USER_LIST", "ROLE_ADMIN, ROLE_USER_EDIT",
						"ROLE_ADMIN, ROLE_USER_REMOVE", request);
				rows[index++] = cols;
			}
			result.setAaData(rows);
		} else {
			result.setAaData(new String[][] {});
		}

		return result;
	}

	private String getSortField(String sIndex) {
		if (StringUtils.hasText(sIndex)) {
			int index = Integer.parseInt(sIndex);
			switch (index) {
				case 1:
					return User_.name.getName();
				case 2:
					return User_.email.getName();
			}
		}
		return User_.name.getName();
	}

	private boolean entityHasErrors(User entity, boolean changePassword, String passwordConfirmation) {
		boolean hasErrors = false;

		if (entity != null) {
			if (validateEmail(entity)) {
				hasErrors = true;
			} else if (changePassword && !entity.getPassword().equals(passwordConfirmation)) {
				hasErrors = true;
			} else if (isValidFileType(entity)) {
				hasErrors = true;
			}
		}

		return hasErrors;
	}

	private boolean validateEmail(User entity) {
		boolean hasError = false;

		User result = userService.findByLogin(entity.getEmail());

		if (result != null && !result.getId().equals(entity.getId())) {
			hasError = true;
		}

		return hasError;
	}

	private String additionalValidation(User entity, boolean changePassword, String passwordConfirmation) {
		StringBuilder message = new StringBuilder();

		if (entity != null) {
			if (validateEmail(entity)) {
				message.append(MessageBundle.getMessageBundle("user.email") + ": " + MessageBundle.getMessageBundle("user.email.duplicate") + "\n <br />");
			}

			if (changePassword && !entity.getPassword().equals(passwordConfirmation)) {
				message.append(MessageBundle.getMessageBundle("user.password.confirmation") + ": "
						+ MessageBundle.getMessageBundle("user.password.confirmatin.failure") + "\n <br />");
			}

			if (isValidFileType(entity)) {
				message.append(MessageBundle.getMessageBundle("user.picturefiles") + ": " + MessageBundle.getMessageBundle("user.fileType") + "\n <br />");
			}
		}

		return message.toString();
	}

	private boolean isValidFileType(User entity) {
		boolean hasErrors = false;

		if (entity.getPictureOriginalName() == null
				|| (entity.getPictureOriginalName() != null && entity.getPictureOriginalName().length() > 0 && !ACCEPTED_FILE_TYPE.contains(fileService
						.extractFileExtension(entity.getPictureOriginalName())))) {
			hasErrors = true;
		}

		return hasErrors;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Set.class, "groups", new CustomCollectionEditor(Set.class) {
			@Override
			protected Object convertElement(Object element) {
				Long id = null;

				if (element instanceof String && !((String) element).equals("")) {
					// From the JSP 'element' will be a String
					try {
						id = Long.parseLong((String) element);
					} catch (NumberFormatException e) {
						log.error("Element was " + ((String) element), e);
					}
				} else if (element instanceof Long) {
					// From the database 'element' will be a Long
					id = (Long) element;
				}

				return id != null ? userGroupService.find(id) : null;
			}
		});
	}

}
