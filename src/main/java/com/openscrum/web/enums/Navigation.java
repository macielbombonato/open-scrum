package com.openscrum.web.enums;

public enum Navigation {

	// @formatter:off

	HOME("/"), 
	INDEX("index"),

	USER("user"),
	USER_INDEX("user/index"),
	USER_NEW("user/new"),
	USER_EDIT("user/edit"),
	USER_LIST("user/list"),
	USER_VIEW("user/view"),
	USER_CHANGE_PASSWORD("user/change-password"),

	USER_PERMISSION("user-group"),
	USER_PERMISSION_LIST("user-group/list"),
	USER_PERMISSION_NEW("user-group/new"),
	USER_PERMISSION_EDIT("user-group/edit"),
	USER_PERMISSION_VIEW("user-group/view"),

	WORKFLOW("workflow"),
	WORKFLOW_LIST("workflow/list"),
	WORKFLOW_NEW("workflow/new"),
	WORKFLOW_EDIT("workflow/edit"),
	WORKFLOW_VIEW("workflow/view"),

	PROJECT("project"),
	PROJECT_INDEX("project/index"),
	PROJECT_NEW("project/new"),
	PROJECT_EDIT("project/edit"),
	PROJECT_LIST("project/list"),
	PROJECT_VIEW("project/view"),

	AUTH("auth"),
	AUTH_LOGIN("auth/login"),
	AUTH_LOGOUT("auth/logout"),
	
	AUDIT_LOG_LIST("auditlog/list"),

	ERROR("error/error"),

	;

	// @formatter:on

	private String path;

	private Navigation(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

}
