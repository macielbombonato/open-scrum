package com.openscrum.business.exception;

public class AttributeNotFoundException extends WorkflowParserException {
	private static final long serialVersionUID = -915127984257044230L;

	public AttributeNotFoundException() {
		super();
	}

	public AttributeNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public AttributeNotFoundException(String message) {
		super(message);
	}

	public AttributeNotFoundException(Throwable throwable) {
		super(throwable);
	}

}
