package com.openscrum.business.exception;

import com.openscrum.common.exception.GenericException;

/**
 * Exception when a workflow definition file couldn't be parsed.
 */
public class WorkflowParserException extends GenericException {
	private static final long serialVersionUID = 4499234621686803976L;

	public WorkflowParserException() {
		super();
	}

	public WorkflowParserException(String message) {
		super(message);
	}

	public WorkflowParserException(Throwable throwable) {
		super(throwable);
	}

	public WorkflowParserException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
