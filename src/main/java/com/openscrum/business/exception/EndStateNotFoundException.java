package com.openscrum.business.exception;

public class EndStateNotFoundException extends WorkflowParserException {
	private static final long serialVersionUID = 9097160171881170251L;

	public EndStateNotFoundException() {
		super();
	}

	public EndStateNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public EndStateNotFoundException(String message) {
		super(message);
	}

	public EndStateNotFoundException(Throwable throwable) {
		super(throwable);
	}

}
