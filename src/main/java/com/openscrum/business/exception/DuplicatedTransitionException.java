package com.openscrum.business.exception;

public class DuplicatedTransitionException extends WorkflowParserException {
	private static final long serialVersionUID = 6804334940368832836L;

	public DuplicatedTransitionException() {
		super();
	}

	public DuplicatedTransitionException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public DuplicatedTransitionException(String message) {
		super(message);
	}

	public DuplicatedTransitionException(Throwable throwable) {
		super(throwable);
	}

}
