package com.openscrum.business.service;

import com.openscrum.data.model.UserStory;

public interface UserStoryService extends BaseService<UserStory, Long> {

}
