package com.openscrum.business.service;

public interface Action {

	void execute();

}
