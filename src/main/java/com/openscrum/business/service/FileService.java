package com.openscrum.business.service;

import java.io.InputStream;

import com.openscrum.business.model.FileContent;
import com.openscrum.data.model.BaseEntity;


public interface FileService<E extends BaseEntity> {
	
	String uploadFile(E entity, FileContent file, InputStream inputStream);
	
	String extractFileExtension(String fileName);
	
}
