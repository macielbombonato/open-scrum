package com.openscrum.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.openscrum.business.service.ProjectService;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.model.Project;
import com.openscrum.data.model.Project_;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.ProjectRepository;

@Service("projectService")
public class ProjectServiceImpl extends BaseServiceImpl<Project, Long> implements ProjectService {
	private static final long serialVersionUID = 6153846668284438323L;

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public List<Project> list() {
		return (List<Project>) projectRepository.findAll(new Sort(Sort.Direction.ASC, Project_.name.getName()));
	}

	@Override
	protected List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields() {
		List<SingularAttribute<? extends BaseEntity, String>> fields = new ArrayList<SingularAttribute<? extends BaseEntity, String>>();
		fields.add(Project_.name);
		fields.add(Project_.description);
		return fields;
	}

	@Override
	protected BaseRepository<Project, Long> getRepository() {
		return projectRepository;
	}

}
