package com.openscrum.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.openscrum.business.service.ReleaseService;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.model.Release;
import com.openscrum.data.model.Release_;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.ReleaseRepository;

@Service("releaseService")
public class ReleaseServiceImpl extends BaseServiceImpl<Release, Long> implements ReleaseService {
	private static final long serialVersionUID = -392945029692971835L;

	@Autowired
	private ReleaseRepository releaseRepository;

	@Override
	public List<Release> list() {
		return (List<Release>) releaseRepository.findAll(new Sort(Sort.Direction.ASC, Release_.name.getName()));
	}

	@Override
	protected List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields() {
		List<SingularAttribute<? extends BaseEntity, String>> fields = new ArrayList<SingularAttribute<? extends BaseEntity, String>>();
		fields.add(Release_.name);
		fields.add(Release_.description);
		return fields;
	}

	@Override
	protected BaseRepository<Release, Long> getRepository() {
		return releaseRepository;
	}

}
