package com.openscrum.business.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.openscrum.business.model.SearchResult;
import com.openscrum.business.service.BaseService;
import com.openscrum.common.exception.AccessDeniedException;
import com.openscrum.common.exception.GenericException;
import com.openscrum.common.exception.NoDataFoundException;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.BaseRepository.SortDirection;

/**
 * Abstract implementation of the {@link BaseService}. All Services must extend this class.
 * 
 * @param <E>
 *            Entity which must extend {@link BaseEntity}.
 */
public abstract class BaseServiceImpl<E extends BaseEntity, ID extends Serializable> implements BaseService<E, ID> {
	private static final long serialVersionUID = 7771676072968791204L;

	@Override
	public SearchResult<E> search(String param) throws NoDataFoundException {
		SearchResult<E> result = new SearchResult<E>();
		result.setResults(getRepository().search(param, buildSearchFields()));
		result.setPageCount(count(param));
		return result;
	}

	@Override
	public SearchResult<E> search(String param, Integer pageSize, Integer pageIndex, String sortField, SortDirection sortDirection) throws NoDataFoundException {
		SearchResult<E> result = new SearchResult<E>();
		result.setResults(getRepository().search(param, buildSearchFields(), pageSize, pageIndex, sortField, sortDirection));
		result.setPageCount(count(param));
		return result;
	}

	@Override
	public int count(String param) {
		return getRepository().count(param, buildSearchFields());
	}

	@Transactional
	@Override
	public E save(E entity) throws GenericException {
		Assert.notNull(entity, "The entity to be persisted must not be null.");
		try {
			return getRepository().saveAndFlush(entity);
		} catch (Exception e) {
			throw new GenericException(e);
		}
	}

	@Transactional
	@Override
	public void remove(E entity) throws AccessDeniedException {
		Assert.notNull(entity, "The entity to be removed must not be null.");
		getRepository().delete(entity);
	}

	@Override
	public int count() {
		return safeLongToInt(getRepository().count());
	}

	@Override
	public List<E> list() {
		return getRepository().findAll();
	}

	@Override
	public E find(ID id) {
		Assert.notNull(id, "The \"id\" must not be null.");
		return getRepository().findOne(id);
	}

	public static int safeLongToInt(long l) {
		if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
			throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
		}
		return (int) l;
	}

	/**
	 * Get the underline repository used by {@link BaseOperationalServiceImpl} concrete implementation.
	 * 
	 * @return A repository, i.e. whom extends {@link OperationalRepository}.
	 */
	protected abstract BaseRepository<E, ID> getRepository();

	/**
	 * Determines which fields consider while filtering.
	 * 
	 * @return A list of fields.
	 */
	protected abstract List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields();

}
