package com.openscrum.business.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openscrum.business.model.FileContent;
import com.openscrum.business.service.FileService;
import com.openscrum.business.service.UserService;
import com.openscrum.common.exception.GenericException;
import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.model.User;
import com.openscrum.data.model.UserGroup;
import com.openscrum.data.model.User_;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.UserRepository;
import com.openscrum.security.CurrentUser;
import com.openscrum.security.UserPermission;

@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {
	private static final long serialVersionUID = -1904549351059422708L;

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FileService<User> fileService;

	// Password encoder.
	private final Md5PasswordEncoder encoder = new Md5PasswordEncoder();

	@Override
	public List<User> list() {
		return userRepository.findAll(new Sort(Sort.Direction.ASC, User_.name.getName()));
	}

	@Override
	public User findByLogin(String login) {
		return userRepository.findUserByEmail(login);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User u = userRepository.findUserByEmail(username);
		Collection<GrantedAuthority> authorities = loadUserAuthorities(u);
		return new CurrentUser(u.getId(), u.getEmail(), u.getPassword().toLowerCase(), u, authorities);
	}

	@Transactional
	@Override
	public User save(User entity) {
		return save(entity, false, null);
	}

	@Transactional
	@Override
	public User save(User user, boolean changePassword, FileContent file) {
		if (changePassword) {
			user.setPassword(encoder.encodePassword(user.getPassword(), null));
		} else {
			User dbUser = this.find(user.getId());
			user.setPassword(dbUser.getPassword());
		}

		if (file != null) {
			if (file != null && file.getFile() != null && file.getFile().getOriginalFilename() != null && !file.getFile().getOriginalFilename().isEmpty()) {

				if (user.getId() == null) {
					userRepository.save(user);
				}

				user.setPictureOriginalName(file.getFile().getOriginalFilename());
				try {
					user.setPictureGeneratedName(fileService.uploadFile(user, file, file.getFile().getInputStream()));
				} catch (IOException e) {
					String message = MessageBundle.getMessageBundle("commons.errorUploadingFile");
					throw new GenericException(message);
				}
			}
		}

		return userRepository.save(user);
	}

	private Collection<GrantedAuthority> loadUserAuthorities(User u) {
		Collection<GrantedAuthority> result = new ArrayList<GrantedAuthority>();

		for (UserGroup group : u.getGroups()) {
			for (UserPermission permission : group.getPermissions()) {
				result.add(new SimpleGrantedAuthority(permission.getAttribute()));
			}
		}

		return result;
	}

	@Override
	protected List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields() {
		List<SingularAttribute<? extends BaseEntity, String>> fields = new ArrayList<SingularAttribute<? extends BaseEntity, String>>();
		fields.add(User_.email);
		fields.add(User_.name);
		return fields;
	}

	@Override
	protected BaseRepository<User, Long> getRepository() {
		return userRepository;
	}

}
