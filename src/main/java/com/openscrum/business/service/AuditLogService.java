package com.openscrum.business.service;

import java.util.List;

import com.openscrum.business.model.SearchResult;
import com.openscrum.common.exception.NoDataFoundException;
import com.openscrum.data.model.AuditLog;
import com.openscrum.data.repository.BaseRepository.SortDirection;

public interface AuditLogService extends BaseService<AuditLog, Long> {
	
	List<AuditLog> list();
	
	AuditLog find(Long id);
	
	AuditLog save(AuditLog entity);
	
	SearchResult<AuditLog> search(String param) throws NoDataFoundException;

	SearchResult<AuditLog> search(String param, Integer pageSize, Integer pageIndex, String sortField, SortDirection sortDirection) throws NoDataFoundException;

}