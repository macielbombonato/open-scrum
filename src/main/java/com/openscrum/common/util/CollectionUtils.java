// $codepro.audit.disable unnecessaryCast
package com.openscrum.common.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * This class contains utilities for collections manipulating.
 */
public class CollectionUtils {

	/**
	 * <p>
	 * This is an utility method to create indexes from given {@link List} into a {@link Map}.
	 * </p>
	 * <p>
	 * To explain how it works, we'll consider an users list.<br>
	 * Users may have properties, such as: <code>username, password, id</code> ... We want to group the users by <code>username</code> for a given users list. To this
	 * end, we call this method passing the users {@link List}. This list will be iterated; each {@link List} element will be inserted into {@link Map}, and the
	 * {@link Map} key will be the property specified by parameted <code>propName</code>, in this case, <code>username</code>.
	 * </p>
	 * <p>
	 * <strong>WARNING:</strong>This method should only be used when <code>propName</code> is unique. If you are not sure about it, use the method <code>groupBy()</code>.
	 * 
	 * @param c
	 *            - {@link List} to be indexed.
	 * @param propName
	 *            - Bean property to be the key for result map.
	 * @param clazz
	 *            - Object class for Map key.
	 * @return a {@link Map} with key defined by parameter 2 (propName) and the object indexed.
	 * @author Rafael M Berne
	 */
	@SuppressWarnings("unchecked")
	public static <K, T> Map<K, T> indexBy(List<T> c, String propName, Class<K> clazz) {

		// verifica se a collection é null, se for retorna uma nova Map
		if (c == null) {
			return new LinkedHashMap<K, T>();
		}

		try {

			Map<K, T> indexed = new LinkedHashMap<K, T>();
			for (Iterator<T> it = c.iterator(); it.hasNext();) {

				T element = it.next();
				// Return the property value using reflection.
				K key = (K) PropertyUtils.getProperty(element, propName);

				indexed.put(key, element);

			}

			return indexed;

		} catch (Exception e) {
			throw new RuntimeException("Error while getting the object property.", e);
		}

	}

	/**
	 * <p>
	 * This method is similar to <code>indexBy()</code>, but it groups the elements that have the same <code>propName</code>.
	 * </p>
	 * <p>
	 * Consequently, the generated {@link Map} will contain the <code>propName</code> as key and the an objects {@link List} which <code>propName</code> is the same.
	 * </p>
	 * 
	 * @param c
	 *            - {@link List} to be indexed.
	 * @param propName
	 *            - Bean property to be the key for result map.
	 * @param clazz
	 *            - Object class for Map key.
	 * @return a {@link Map} with <code>propName</code> as key and a {@link List} as value.
	 * @author Rafael M Berne
	 */
	@SuppressWarnings("unchecked")
	public static <K, T> Map<K, List<T>> groupBy(List<T> c, String propName, Class<K> clazz) {

		if (c == null) {
			return new LinkedHashMap<K, List<T>>();
		}

		try {

			Map<K, List<T>> grouped = new LinkedHashMap<K, List<T>>();
			for (Iterator<T> it = c.iterator(); it.hasNext();) {
				T element = it.next();

				K key = (K) PropertyUtils.getProperty(element, propName);

				List<T> l = (List<T>) grouped.get(key);
				if (l == null) {
					l = new ArrayList<T>();
					grouped.put(key, l);
				}

				l.add(element);
			}

			return grouped;

		} catch (Exception e) {
			throw new RuntimeException("Error while getting the object property.", e);
		}

	}

	/**
	 * Given a {@link Collection} of type X that contains a property with <code>propertyName</code>, this method will produce another {@link Collection} of type Y, that's
	 * the type of <code>propertyName</code>.
	 * 
	 * @param c
	 *            The collection that a property list will be extracted.
	 * @param propertyName
	 *            The property to be extracted.
	 * @param result
	 *            The resultant list.
	 * @return The resultant list specified by <code>result</code> parameter.
	 * @author Rafael M Berne
	 */
	@SuppressWarnings("unchecked")
	public static <T> Collection<T> pluck(Collection<?> c, String propertyName, Collection<T> result) {
		if (c == null)
			return result;
		try {
			for (Iterator<?> it = c.iterator(); it.hasNext();) {
				Object element = it.next();
				T value = (T) PropertyUtils.getProperty(element, propertyName);
				result.add(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Same as <code>pluck(Collection c, String propertyName, Collection result)</code>, but for {@link List} instead of {@link Collection}.
	 * 
	 * @author Rafael M Berne
	 */
	public static <T> List<T> pluckList(Collection<?> c, String propertyName) {
		return (List<T>) pluck(c, propertyName, new ArrayList<T>());
	}

	/**
	 * Same as <code>pluck(Collection c, String propertyName, Collection result)</code>, but for {@link Set} instead of {@link Collection}.
	 * 
	 * @author Rafael M Berne
	 */
	public static <T> Set<T> pluckSet(Collection<?> c, String propertyName) {
		return (Set<T>) pluck(c, propertyName, new HashSet<T>());
	}

	@SuppressWarnings("unchecked")
	public static <T> void joinCollections(Collection<T> result, Collection<T>... collections) {
		for (Collection<T> collection : collections) {
			result.addAll(collection);
		}
	}

	/**
	 * This method is used to copy all object contents between two lists. Notice which only the matched property names with same types will be copied.
	 * 
	 * @param listDest
	 *            The target list to be filled.
	 * @param listSource
	 *            The source list to provide content.
	 * @param destClass
	 *            Class that defines the target object.
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static <T, S> void copyList(List<T> listDest, List<S> listSource, Class<T> destClass) throws InstantiationException, IllegalAccessException,
			InvocationTargetException {
		for (S source : listSource) {
			T dest = destClass.newInstance();
			BeanUtils.copyProperties(dest, source);
			listDest.add(dest);
		}
	}

	public static <T> Collection<T> filter(Collection<T> c, final String propertyName, final Object propertyValue) {
		return filter(c, new Filter<T>() {
			public boolean accept(T o) {
				Object prop = null;
				try {
					prop = PropertyUtils.getProperty(o, propertyName);
				} catch (Exception e) {
					// do nothing.
				}
				return equals(prop, propertyValue);
			}

			private boolean equals(Object a, Object b) {
				return (a == null) ? b == null : a.equals(b);
			}
		});
	}

	public static <T> Collection<T> filter(Collection<T> c, Filter<T> f) {
		if (c == null)
			return c;
		for (Iterator<T> it = c.iterator(); it.hasNext();) {
			T element = it.next();
			if (!f.accept(element)) {
				it.remove();
			}
		}
		return c;
	}

	public interface Filter<T> {
		public boolean accept(T o);
	}

	public static <T> List<T> find(Collection<T> c, String property, Object value) {
		ArrayList<T> r = new ArrayList<T>();
		if (c != null) {
			try {
				for (T o : c) {
					Object v = PropertyUtils.getProperty(o, property);
					boolean equals = (value == null) ? (v == null) : value.equals(v);
					if (equals)
						r.add(o);
				}
			} catch (Exception e) {
				// Do nothing.
			}
		}
		return r;
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> find(Collection<?> c, Class<T> filterClass) {
		ArrayList<T> r = new ArrayList<T>();
		if (c != null) {
			for (Object o : c) {
				if (filterClass.isAssignableFrom(o.getClass()))
					r.add((T) o);
			}
		}
		return r;
	}

	public static <T> T findFirst(Collection<?> c, Class<T> filterClass) {
		List<T> l = find(c, filterClass);
		if (l.isEmpty())
			return null;
		return l.get(0);
	}

	public static Object findFirst(Collection<?> c, String property, Object value) {
		List<?> r = find(c, property, value);
		if (r.isEmpty())
			return null;
		return r.get(0);
	}

}
