package com.openscrum.common.config.model;

public class ApplicationProperties {
	
	private String uploadedFilesPath;
	
	private String filesHandler;
	
	public String getUploadedFilesPath() {
		return uploadedFilesPath;
	}

	public void setUploadedFilesPath(String uploadedFilesPath) {
		this.uploadedFilesPath = uploadedFilesPath;
	}

	public String getFilesHandler() {
		return filesHandler;
	}

	public void setFilesHandler(String filesHandler) {
		this.filesHandler = filesHandler;
	}

}
