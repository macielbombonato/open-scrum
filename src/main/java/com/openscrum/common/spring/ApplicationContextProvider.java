package com.openscrum.common.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * This class provides application-wide access to the Spring ApplicationContext. The ApplicationContext is injected by the class
 * "ApplicationContextProvider".
 * 
 * @author Siegfried Bolz
 */
public class ApplicationContextProvider implements ApplicationContextAware {

    /**
     * Injected from the class "ApplicationContextProvider" which is automatically loaded during Spring-Initialization.
     */
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        // Wiring the ApplicationContext into a static method
		AppContext.setApplicationContext(ctx);
	}

}
