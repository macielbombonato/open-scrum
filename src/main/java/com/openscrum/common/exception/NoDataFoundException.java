package com.openscrum.common.exception;

public class NoDataFoundException extends GenericException {

	private static final long serialVersionUID = 6102557498006358663L;

	public NoDataFoundException() {
		super();
	}

	public NoDataFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoDataFoundException(String customMsg) {
		super(customMsg);
	}

	public NoDataFoundException(Throwable cause) {
		super(cause);
	}

}