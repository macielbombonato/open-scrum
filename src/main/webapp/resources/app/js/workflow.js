var workflowDrawer = {};
(function() {
	/*
	 * Build the nodes (using workflow states and transitions).
	 */
	var buildNodes = function(workflow) {
		var root_nodes = [];
		var unresolved_actions = [];
		var states = workflow.states;
		for (var i = 0; i < states.length; i++) {
			var title = $.trim(states[i].name);
			var start_level = states[i].isStart ? 1 : 2;

			var node = {
				title : title,
				level : start_level,
				id : "node_" + i,
				database_id: states[i].id,
				actions : []
			};
			
			if (states[i].isFinal) node.isFinal = true;

			// build child nodes
			var transitions = states[i].transitions;
			if (transitions && transitions.length > 0) {
				for (var t = 0; t < transitions.length; t++) {
					var action = {
							title : $.trim(transitions[t].name),
							node : sub_node
					};
					
					// add the node to be resolved later
					unresolved_actions[unresolved_actions.length] = {
						node : node,
						action : action,
						targetName : $.trim(transitions[t].target)
					};
				}
			}
			
			// must be a brand new root node or the first ever
			root_nodes.push(node);
		}
		
		// try resolve missing action nodes
		for (var i = unresolved_actions.length - 1; i >= 0; i--) {
			var sub_node = getChildrenNode(root_nodes, unresolved_actions[i].targetName);
			if (sub_node) {
				if (sub_node.database_id > unresolved_actions[i].node.database_id)
					sub_node.level = unresolved_actions[i].node.level + 1;
				unresolved_actions[i].action.node = sub_node;
				unresolved_actions[i].node.actions.push(unresolved_actions[i].action);
			}
		}


		return root_nodes;
	};
	
	var getChildrenNode = function(nodes, title) {
		if (!nodes || !title) return;

		var result = $.grep(nodes, function(n) {
			return n.title == title;
		});
		if (result) {
			return result[0];
		}
	};

	var setFramesAndIds = function(all) {
		var max_level = getMaxLevel(all);
		var sets = [], y_pad = [0];
		
		for (var i = 1; i <= max_level; i++) {
			sets.push($.grep(all, function(n) { return n.level == i; }));
			y_pad.push(y_pad[i - 1] + .2);
		}
		for(var i = 0; i < sets.length; i++) {
			var nodes = sets[i];
			var w = 1.0 / (nodes.length + 3);
			var x_pad = (1.0 - (w * nodes.length)) / (nodes.length + 1.0);
			for (var j = 0; j < nodes.length; j++) {
				var node = nodes[j];
				node.frame = {
					x : (x_pad * (j + 1) + w * j),
					y : y_pad[i],
					w : w,
					h : 50
				};
			}
		}
	};
	
	var getMaxLevel = function(nodes) {
		var result = 0;
		var endNode = null;
		for (var i = 0; i < nodes.length; i++) {
			if (nodes[i].level > result) result = nodes[i].level;
			if (nodes[i].isFinal) endNode = nodes[i];
		}
		if (endNode) endNode.level = ++result;
		return result;
	};

	var drawNodes = function(nodes) {
		var chart = $(".chart").empty();
		var h_percent_to_pixels = function(percent) {
			return Math.round(percent * chart.width()) + "px";
		};
		var v_percent_to_pixels = function(percent) {
			return Math.round(percent * chart.height()) + "px";
		};
		for (var i = 0; i < nodes.length; i++) {
			var node = nodes[i];
			var div = $("<div></div>").addClass("node").css({
				left : h_percent_to_pixels(node.frame.x),
				top : v_percent_to_pixels(node.frame.y),
				width : h_percent_to_pixels(node.frame.w),
				height : node.frame.h + "px"
			}).html(node.title).attr("id", node.id).appendTo(chart).textfill({
				offset : -4
			});
			var fontSize = div.find("span").css("fontSize");
			div.empty();
			div.append($("<div></div>").html(node.title).css("fontSize",
					fontSize).addClass("title"));
		}
	};

	var drawConnections = function(nodes) {
		jsPlumb.reset();
		for (var i = 0; i < nodes.length; i++) {
			var node = nodes[i];

			for ( var j = 0; j < node.actions.length; j++) {
				var action = node.actions[j];

				jsPlumb.connect({
					source : node.id,
					target : action.node.id,
					anchors : [ "AutoDefault", "AutoDefault" ],
					paintStyle : {
						lineWidth : 2,
						strokeStyle : "#44aaff",
						outlineColor : "#0066ff",
						outlineWidth : 1
					},
					connector : [ "Bezier", {
						curviness : 80
					} ],
					endpoint : [ "Dot", {
						radius : 5
					} ],
					overlays : [ [ "PlainArrow", {
						width : 20,
						length : 15,
						location : .35,
						paintStyle : {
							strokeStyle : "#0066ff",
							lineWidth : 1
						}
					} ], [ "Label", {
						label : action.title || " ",
						cssClass : "action",
						location : 0.30
					} ] ]
				});
			}
		}
	};

	workflowDrawer.draw = function(workflow) {
		var roots = buildNodes(workflow);
		setFramesAndIds(roots);
		drawNodes(roots);
		drawConnections(roots);

	};

})();
