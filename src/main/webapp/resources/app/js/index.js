$(document).ready(function() {
	$.getJSON(App.contextPath + "/project/listProjects", function(data) {
		if (data.length > 0) {
			var table = $("#projects");

			$.each(data, function(key, val) {
				table.append("<li class=\"list-group-item\"><a href=\"sprintboard/stories/" + val.id + "\">" + val.name + "</a></li>");
			});
		}
		return;
	});
});