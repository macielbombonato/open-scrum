package com.openscrum.data.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Project.class)
public abstract class Project_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SingularAttribute<Project, Date> startDate;
	public static volatile SetAttribute<Project, User> users;
	public static volatile SingularAttribute<Project, Date> extendedDate;
	public static volatile SingularAttribute<Project, String> description;
	public static volatile SingularAttribute<Project, String> name;
	public static volatile SingularAttribute<Project, Date> endDate;

}

