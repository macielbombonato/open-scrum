package com.openscrum.data.model;

import com.openscrum.data.enums.WorkflowStateType;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(State.class)
public abstract class State_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SetAttribute<State, Transition> transitions;
	public static volatile SingularAttribute<State, String> name;
	public static volatile SingularAttribute<State, WorkflowStateType> type;
	public static volatile SingularAttribute<State, Workflow> workflow;

}

