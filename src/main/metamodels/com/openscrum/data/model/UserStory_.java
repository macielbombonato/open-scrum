package com.openscrum.data.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserStory.class)
public abstract class UserStory_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SingularAttribute<UserStory, Double> storyPoints;
	public static volatile SingularAttribute<UserStory, Project> project;
	public static volatile SingularAttribute<UserStory, Integer> priority;
	public static volatile SingularAttribute<UserStory, String> description;
	public static volatile SingularAttribute<UserStory, String> name;
	public static volatile SingularAttribute<UserStory, Release> release;

}

